class User {
  constructor(name,score){
    this.name=name;
    this.score=score;
  }
}

function randomInt(min,max){
  return Math.round(Math.random()*(max-min)+min);
}

class UsersDB {
  constructor(){
    this.users=[];
    this.addedId=0;
  }
  addUser(name, score){
    let user=new User(name,score)
    this.users[this.addedId]=user;
    return this.addedId++;
  }
  addRandomUser(){
    this.addUser('Name-'+randomInt(1,10),randomInt(0,100));
  }
  addDemoData(count=10){
    for (let i=0;i<count;i++){
      this.addRandomUser();
    }
  }
  getUser(id){
    return this.users[id];
  }
  getAll(){
    let arr = [];
    var i=0;
    this.users.forEach(function(item, index){
      if (item!=null){
      arr[i]=item;
      arr[i++]['id']=index;
    }
    });
    return arr;
  }
  deleteUser(id){
    this.users[id]=null;
  }
  updateUser(id,name,score){
    this.users[id] = new User(name, score);
  }
  userExists(id){
    return this.users[id]&&(this.users[id]!=null);
  }
}

module.exports = {
  UsersDB,
  User
};
