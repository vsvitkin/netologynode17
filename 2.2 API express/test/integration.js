const supertest = require('supertest');
const assert = require('assert');
const should = require('chai').should();
const bodyParser = require('body-parser');

describe('Testing homework 2.2',()=>{
  it('Should always pass',()=>{});
  describe('Testing REST API user',()=>{
    let server;
    before(done=>{
      require('../index.js');
      setTimeout(()=>{
        server = supertest.agent('http://localhost:5000');
        done();
      }, 1000);
    });

    it('Error without path',(done)=>{
      server
      .get('/')
      .end((err,res)=>{
        assert(res.status==404);
        done();
      });
    });
    let url='/users';
    describe('User add',()=>{
      describe('With wrong args',()=>{
        [{},{name:"Vasily"},{score:"123"},{name:"",score:""}].forEach(body=>{
          it('Adding with wrong body '+JSON.stringify(body), done=>{
            server
            .post(url)
            .send(body)
            .end((err,res)=>{
              res.status.should.be.equal(400);
              assert.deepEqual(res.body, {error: 'Name and score are required for this operation'});
              done();
            });
          });
        });
      });
      describe('With correct args',()=>{
        [{name:"Vasily",score:"1"},{score:123, name:"Test"},{score:"123", name:"Test"},{name:"Ф",score:"12"}].forEach(body=>{
          it('Adding with correct body '+JSON.stringify(body), done=>{
            server
            .post(url)
            .send(body)
            .end((err,res)=>{
              res.status.should.be.equal(200);
              res.body.should.have.property('id').and.to.be.a('number');
              done();
            });
          });
        });
      });
    });
    describe('User delete', ()=>{
      describe('With wrong args',()=>{
        [url,url+'/',url+'/qwe',url+'/qwe/',url+'/123/123/123','/'+url+'/qwe'].forEach(path=>{
          it('Adding with wrong url '+path, done=>{
            server
            .delete(path)
            .end((err,res)=>{
              res.status.should.be.equal(404);
              res.body.should.have.property('error');
              done();
            });
          });
        });
      });
      describe('With correct args',()=>{
        [url+'/1',url+'/2',url+'/5',].forEach(path=>{
          it('Adding with correct url '+path, done=>{
            server
            .delete(path)
            .end((err,res)=>{
              res.status.should.be.equal(200);
              res.body.should.be.deep.equal({});
              done();
            });
          });
        });
      });
    });

  });
});
