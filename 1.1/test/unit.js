const assert = require('assert');

const Pokemon = require('../pokemon.js').Pokemon;
const PokemonList = require('../pokemonList.js').PokemonList;

function assertUndefined(val){
  assert(typeof val !=='undefined','Class is undefined');
}

function getTestPokemonList(){
  return new PokemonList(new Pokemon('AAA', 10), new Pokemon('ccc', 25));
}

describe('Testing homework 1.1',()=>{
  it('Should always pass',()=>{});
  describe('Testing class Pokemon',()=>{

    it('Class exists',()=>{
      assertUndefined(Pokemon);
    });
    it('Show should return correct values',()=>{
      assert.deepEqual(
        (new Pokemon('Name',99)).show(),
        {name:'Name',power:99}
      );
    });
  });

  describe('Testing class PokemonList',()=>{
    it('Class exists',()=>{
      assertUndefined(PokemonList);
    });

    it('Constructor test',()=>{
      const p = new PokemonList(new Pokemon('BBB', 20));
      assert.deepEqual(p[0], {name:'BBB',power:20});
    });

     it('Add test',()=>{
       const p = new PokemonList();
       p.add('AAA', 10);
       assert.deepEqual(p[0],{name:'AAA',power:10});
     });

     it('Show test',()=>{
       assert.deepEqual((getTestPokemonList()).show(),[{name:'AAA',power:10},{name:'ccc',power:25}]);
     });

     it('Max test',()=>{
       assert.deepEqual((getTestPokemonList()).max(),{name:'ccc',power:25});
     });


  });
});
