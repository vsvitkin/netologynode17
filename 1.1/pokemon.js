class Pokemon {
  constructor (name, power) {
    this.name=name;
    this.power=power;
  }
  show(){
    //console.log(`Hello, I'm pokemon ${this.name}, my power is ${this.power}`);
    return {'name':this.name,'power':this.power};
  }
  //*Переопределите и используйте метод valueOf у покемонов, для решения этой задачи.
  valueOf(){
    return this.power;
  }
}

module.exports = {
  Pokemon
}
