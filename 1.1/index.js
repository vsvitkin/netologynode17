/************************
Vasily Svitkin 2017
Netology NodeJS course
Home work 17
*/

const Pokemon = require('./pokemon.js').Pokemon;
// Класс списка покемонов
const PokemonList = require('./pokemonList.js').PokemonList;

let fish = new Pokemon('Fish', 10);
let dog = new Pokemon('Dog',3);
//Список покемонов, в конструкторе добавляем сразу двух покемонов

let lost = new PokemonList(fish, dog);
let found = new PokemonList();

//*Добавить несколько новых покемонов в каждый список.
lost.add('Bird',7);
lost.add('Turtle',4);
found.add('Cat',2);
found.add('Bear',9);

//*Перевести одного из покемонов из списка lost в список found
found.push(lost.pop());

//Выводим список покемонов
console.log('Lost list:');
console.log(lost.show());
console.log('Found list:');
console.log(found.show());
console.log('Best in lost:');
console.log(lost.max().show());
console.log('Best in found:');
console.log(found.max().show());
