const Pokemon = require('./pokemon.js').Pokemon;

class PokemonList extends Array{
  constructor (...pokemons){
    super();
    pokemons.forEach(p=>{this.push(p)});
  }
  add(name, power){
    this.push(new Pokemon(name, power));
  }
  //*Добавить спискам покемонов метод show, который выводит информацию о покемонах и их общее количество в списке.
  show(){
    //console.log('Count of pokemons in list : '+this.length);
    return this;
  }
  //*Добавить спискам покемонов метод max, который возвращает покемона максимального уровня.
  max(){
    let pokemon=null;;
    this.forEach(p=>{
      if ((pokemon==null)||(p>pokemon)){
        pokemon=p;
      }});
    return pokemon;
  }
}

module.exports = {
  PokemonList
}
